<?php
require_once("action/IndexAction.php");
$action = new IndexAction();
$action->execute();
require_once("partial/header.php");

$communityPostCount = $action->communityPostCount;
$start = $action->start;
$hasNextPage = $action->hasNextPage;
$postList = $action->postList;
?>


<main>
	<div id ="subHeader"> 
		<h1 id="communityName">ACCUEIL DE READ-IT</h1>
		<p id="headerText"><?= $trans->read("index", "headerMessage") ?></p>
	</div>
<div id = "postWrapper"> <!-- Ici les div vont append du PHP-->
	<?php 
	for ($i = 1; $i <= count($postList); $i++) {?>
		<div <?php if($i % 2 == 0){?>style ="background-color: #e6e6e6" <?php }	 ?>class="aPost">
				<div class = "leftPostSection">
				<!--<div class="noPost"><?= $postList[$i]["inside_message"]?></div> -->
					<div class="rank">  <?php echo $i + ($start) ?>. </div>
					<div class="rightOfLeftSection">	
							<div class="clearboth" id = "upvoteIcon">
							<?php 
							if (isset($_SESSION["user_id"])) {
								if ($_SESSION["user_id"]>0) {
									if($postList[$i]["vote"] == 1) {
							 ?>
								<a href="#" onclick="upvotePost(event, <?= $postList[$i]["id"] ?>, <?= $_SESSION["user_id"] ?>, <?= $postList[$i]["user_id"]?>)">
									<img class="arrows" src="images/upvotedArrow.png" alt="upvote arrow"/> 
								</a>
								<?php  
								}
								else
									{ ?>
								<a href="#" onclick="upvotePost(event, <?= $postList[$i]["id"] ?>, <?= $_SESSION["user_id"] ?>, <?= $postList[$i]["user_id"]?>)">
										<img class="arrows" src="images/upArrow.png" alt="upvote arrow"/> 
								</a>
								<?php }
							}
							else{
							?>
								<a href="#" onclick="">
									<img class="arrows" src="images/upArrow.png"/> 
								</a>
							<?php
							}
						}
						else {
							?>
								<a href="#" onclick="">
									<img class="arrows" src="images/upArrow.png" /> 
								</a>
							<?php
						}
								?>			
							</div>
							<span> <?= $postList[$i]["score"]?> </span>
							<div class="clearboth" id = "downvoteIcon">
							<?php 
							if (isset($_SESSION["user_id"])) {
								if ($_SESSION["user_id"]>0) {
									if($postList[$i]["vote"] == -1){
							 ?>
								<a href="#" onclick="downvotePost(event, <?= $postList[$i]["id"] ?>, <?= $_SESSION["user_id"] ?>, <?= $postList[$i]["user_id"]?>)">
									<img class="arrows" src="images/downvotedArrow.png"/> 
								</a>
								<?php  
								}
								else
									{ ?>
								<a href="#" onclick="downvotePost(event, <?= $postList[$i]["id"] ?>, <?= $_SESSION["user_id"] ?>, <?= $postList[$i]["user_id"]?>)">
									<img class="arrows" src="images/downArrow.png"/> 
								</a>
								<?php }
							}
							else{
							?>
								<a href="#" onclick="">
									<img class="arrows" src="images/downArrow.png" /> 
								</a>
							<?php
							} 
						}
						else {
							?>
								<a href="#" onclick="">
									<img class="arrows" src="images/downArrow.png" /> 
								</a>
							<?php
						}
								?>			
							</div>
					</div>
				</div>
				<div class = "rightPostSection">
						<h3 id="titlePost">
						<?php
						$postId = "/read-it/r/post/comments.php?post_id=" . $postList[$i]["id"] . "&community_id=" . $postList[$i]["community_id"];
						?>
						<?php 	
							if($text = preg_match(REGEX_URL,$postList[$i]["outside_link"])){
						 ?>
							<a href= <?= $postList[$i]["outside_link"]?> > <?= htmlentities($postList[$i]["titre"])?>  </a>
								<?php 	 
								}
								else
								{
								?>
							<a href= <?= $postId ?> > <?= htmlentities($postList[$i]["titre"])?> <span class="selfPost">self</span> </a>
							<?php 	
							}
						?>
						<?php 	if($_SESSION["visibility"]==2){?>
							<span class="adminOption">
								<form action="" method="POST" onsubmit="return confirm('Are you sure you want to submit this form?');">
									<input type="hidden" name="post_id_to_delete" value=<?=$postList[$i]["id"]?>></input>
									<input name="toDelete" value="X" class = "adminDelete" type = "submit"/>
								</form>
							</span>
						<?php	} ?>
						
						</h3>
						<p class="tagline"><?= $trans->read("post", "whenSummited") ?><?= $postList[$i]["date_creation"]?> <?= $trans->read("post", "who") ?> <a href= <?="/read-it/u/user.php?user_id=" . $postList[$i]["user_id"]?>><?= $postList[$i]["username"]?> </a> <?= $trans->read("post", "in") ?> /r/<?= $postList[$i]["nomCommunity"]?></p>
						<a class = "comments" href=<?= $postId ?>><?= $trans->read("post", "comments") ?></a>
				</div>
				
		</div>
		<?php } ?>
	</div>
	<div id = "sideBar" >
	</div>
	<div id = "subFooter">
		<?php if ($hasNextPage) {?>
		<div class = "clearboth"> <?= $trans->read("subFooter", "seeMore")?>
		<form action="index.php"> 
		<?php
		if (empty($_GET)) {
		?>
			<input type="hidden" name="pagenumber" value="2"></input>
		<?php
		}
		elseif (isset($_GET["pagenumber"])) {
		?>	
			<input type="hidden" name="pagenumber" value=<?= $_GET["pagenumber"]+1 ?>></input>
		<?php
		}
		else {
			?>
			<input type="hidden" name="pagenumber" value=2></input>
			<?php
		}
		?>
		<input type="submit" value=<?=$trans->read("subFooter", "btnNextPage")?> />
		<?php
		}
		?>
		</form>
		</div>
	</div>
</main>
<?php  
require_once("partial/footer.php");
