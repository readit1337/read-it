<?php
require_once("../../action/MessageAction.php");
$action = new MessageAction();
$action->execute();
require_once("../../partial/header.php");

?>

<main>
	<div id="creationPost" class="creationPost">
		<form  id = "newPost" class= "newPost" name="newPostForm" method="post">
			<div>Titre du message<input type="text" name="titreNouveauMessage" placeholder="Titre" ></div>
			<div>Lien externe (optionnel)<input type="text" name="messageLienExterne" placeholder="http:\\..."></div>
			<div>Contenue (optionnel)<textarea name="messageContenue" id="messageContenue"> </textarea></div>
	        <script>
	        CKEDITOR.replace( 'messageContenue' );		
	        </script>
			<input type="submit">
		</form>
	</div>
</main>
<?php  
require_once("../../partial/footer.php");
