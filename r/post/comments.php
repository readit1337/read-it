<?php
require_once("../../action/CommentsAction.php");
$action = new CommentsAction();
$action->execute();
require_once("../../partial/header.php");

// Faut get les infos du comments selon le get id du comment/post
$postInfo =$action->postInfo; 
$community = $action->community;
$commentList =  $action->commentList; 

?>

<main>
	<div>

			<div id ="subHeader">	
				<h1 id="communityName"><?=$community["name"] ?></h1>
				<p id="headerText"><?= $community["header_text"]?></p>
			</div>

		<div class="aMessage"><!--injecter php -->
				<div class = "leftPostSection">
				<!--<div class="noPost"><?= $postList[$i]["inside_message"]?></div> -->

					<div class="clearboth" id = "upvoteIcon">
						<a href="#" onclick="upvotePost(event, <?= $_GET["post_id"] ?>, <?= $_SESSION["user_id"] ?>, <?= $community["user_id"]?>)">
							<img id="upvote-post" class="arrows" src="../../images/upArrow.png" alt="upvote arrow"/> 
						</a>			
					</div>
					<span class="rank"></span><span> <?= $postInfo["score"]?> </span>
					<div class="clearboth" id = "downvoteIcon">
						<a href="#" onclick="downvotePost(event, <?= $_GET["post_id"] ?>, <?= $_SESSION["user_id"] ?> , <?= $community["user_id"]?>)">
							<img id="downvote-post" class="arrows" src="../../images/downArrow.png" alt="downvote arrow"/> 
						</a>			
					</div>
				</div>
				<div class = "rightPostSection">
						<h3 id="titlePost">
							<a href=""><?= $postInfo["titre"]?></a>
						</h3>
						<p class="tagline"><?= $trans->read("post", "whenSummited") ?><?= $postInfo["dateCreation"]?> <?= $trans->read("post", "who") ?> <?= $postInfo["username"]?> dans /r/<?= $postInfo["communityName"]?></p>
						
						
				</div>
				<div id="messageWrapper">
							<?= $postInfo["inside_message"]?>
				</div>
		</div>
	</div>
	<div id="commentsWrapper">
		<h2 id="commentsTitle"> <?= $trans->read("post", "comments") ?></h2>
		<div id="userComments">
			
			<p id="comments">
			</p>
			<?php  	
				$id = 1;
				foreach ($commentList as $comment) {
					?>
					<div id=<?= "comment-" . $id  ?> class="commentWrapper">
						<div class="voteComment">

								<div class="clearboth upvoteIconComment" >
								<?php
								if ($_SESSION["logged"]) {
								if($commentList[$id]["vote"] == 1){
								?>
								<a href="#" onclick="upvoteComment(event, <?= $comment["id"] ?>, <?= $_SESSION["user_id"]?>, <?= $comment["user_id"]?>)">
								<img id="upvote-post" class="arrows" src="../../images/upVotedArrow.png" alt="upvote arrow"/>
								</a>	
								<?php
								}
								else{
								?>
								<a href="#" onclick="upvoteComment(event, <?= $comment["id"] ?>, <?= $_SESSION["user_id"] ?>, <?= $comment["user_id"]?>)">
									<img id="upvote-post" class="arrows" src="../../images/upArrow.png" alt="upvote arrow"/>
								</a>	
								<?php
									}
								}
								?>	
							</div>
							<div class="clearboth scoreComment"><?= $comment["score"]?></div>
							<div class="clearboth downvoteIconComment" >
								<?php
								if ($_SESSION["logged"]) {
								if($commentList[$id]["vote"] == -1){
								?>
								<a href="#" onclick="downvoteComment(event, <?= $comment["id"] ?>, <?= $_SESSION["user_id"] ?> , <?= $comment["user_id"]?>)">
									<img id="downvote-post" class="arrows" src="../../images/downVotedArrow.png" alt="downvote arrow"/> 
								</a>
								<?php
								}
								else{
								?>
								<a href="#" onclick="downvoteComment(event, <?= $comment["id"] ?>, <?= $_SESSION["user_id"] ?>, <?= $comment["user_id"]?> )">
									<img id="downvote-post" class="arrows" src="../../images/downArrow.png" alt="downvote arrow"/> 
								</a>
								<?php
									}
								}
								?>			
							</div>
						</div>
						<div class="rightComment">	
						<div class="whomComment">	
							<span class="comment-date" id=<?= "comment-date-" . $id ?>>  <?= $trans->read("post", "whenSummited") ?>  <?= $comment["date_creation"] ?> </span>
							<span class="commenteur" id=<?= "commenteur-" . $id ?>> <?= $trans->read("post", "who") ?> <a href=<?= "/read-it/u/user.php?user_id=" . $comment["user_id"]?>> <?= $comment["username"] ?> </a> </span>
						</div>
							<p class="commentaire-text" id=<?= "comment-text-" . $id ?>> <?= $comment["comment_text"] ?> </p>
						</div>

					<?php 	if($_SESSION["visibility"]>=2){?>
							<span class="adminOption">
								<form action="" method="POST" onsubmit="return confirm('Are you sure you want to submit this form?');">
									<input type="hidden" name="comment_id_to_delete" value=<?=$comment["id"]?> ></input>
									<input name="toDelete" value="X" class = "adminDelete" type = "submit"/>
								</form>
							</span>
					<?php	} ?>

					</div>
					<?php
					$id++;
				}
			?>
		</div>
	</div>
	<?php  
	if ($_SESSION["logged"]) {
	?>
    <form name="post-comment" id="post-comment" class="post-comment" action="#" method="post">
            <textarea name="editor1" id="editor1" rows="10" cols="80" form="post-comment">
            </textarea>
            <input type="submit">Soumettre le message</input>
            <input type="hidden" name="hidden" id="hidden" value=<?= $_GET["post_id"]?>>
            <script>
                // Replace the <textarea id="editor1"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('editor1');
            </script>
        </form>
    <?php
}
	?>
</main>
<?php  
require_once("../../partial/footer.php");
