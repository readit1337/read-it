DROP TABLE `web_comments`,
`web_community`,
`web_messagerie`,
`web_post`,
`web_user`,
`web_user_comments`,
`web_user_post`;


CREATE TABLE `readit`.`web_user` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `email` VARCHAR(255) NOT NULL ,
  `username` VARCHAR(64) NOT NULL ,
  `password` VARCHAR(255) NOT NULL ,
  `visibility` INT(2) NOT NULL ,
  `IMG_PATH` VARCHAR(255) NULL DEFAULT NULL ,
  `karma` INT NOT NULL ,
  PRIMARY KEY (`id`),
  UNIQUE `idx_unique_user_email` (`email`(255)),
  UNIQUE `idx_unique_user_username` (`username`(64)))
  ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci COMMENT = 'Web_User table';

CREATE TABLE `readit`.`web_community` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `name` VARCHAR(64) NOT NULL ,
  `side_text` VARCHAR(2500) NOT NULL ,
  `header_text` VARCHAR(2500) NOT NULL ,
  PRIMARY KEY (`id`),
  CONSTRAINT FOREIGN KEY (`user_id`) REFERENCES `web_user`(`id`))
  ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci COMMENT = 'web_community table';

CREATE TABLE `readit`.`web_post` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `community_id` INT NOT NULL ,
  `titre` VARCHAR(500) NOT NULL ,
  `inside_message` VARCHAR(3750) NULL ,
  `date_creation` TIMESTAMP NOT NULL ,
  `outside_link` VARCHAR(500) NULL ,
  `score` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT FOREIGN KEY (`community_id`) REFERENCES `web_community`(`id`),
  CONSTRAINT FOREIGN KEY (`user_id`) REFERENCES `web_user`(`id`))
  ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci COMMENT = 'web_post table';

CREATE TABLE `readit`.`web_comments` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `community_id` INT NOT NULL ,
  `post_id` INT NOT NULL ,
  `date_creation` TIMESTAMP NOT NULL ,
  `comment_text` VARCHAR(2500) NOT NULL ,
  `score` INT NOT NULL ,
  PRIMARY KEY (`id`),
  CONSTRAINT FOREIGN KEY (`user_id`) REFERENCES `web_user`(`id`),
  CONSTRAINT FOREIGN KEY (`community_id`) REFERENCES `web_community`(`id`) ON DELETE CASCADE,
  CONSTRAINT FOREIGN KEY (`post_id`) REFERENCES `web_post`(`id`) ON DELETE CASCADE)
  ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci COMMENT = 'web_comments table';

  CREATE TABLE `readit`.`web_user_post` (
    `post_id` INT NOT NULL ,
    `user_id` INT NOT NULL ,
    `vote` INT NOT NULL ,
    CONSTRAINT FOREIGN KEY (`post_id`) REFERENCES `web_post`(`id`) ON DELETE CASCADE,
    CONSTRAINT FOREIGN KEY (`user_id`) REFERENCES `web_user`(`id`))
    ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci COMMENT = 'web_user_post table';

  CREATE TABLE `readit`.`web_user_comments` (
    `user_id` INT NOT NULL ,
    `comment_id` INT NOT NULL ,
    `vote` INT NOT NULL,
    CONSTRAINT FOREIGN KEY (`comment_id`) REFERENCES `web_comments`(`id`) ON DELETE CASCADE,
    CONSTRAINT FOREIGN KEY (`user_id`) REFERENCES `web_user`(`id`))
    ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci;

  CREATE TABLE `readit`.`web_messagerie` (
    `id_from` INT NOT NULL ,
    `id_to` INT NOT NULL ,
    `message` VARCHAR(3750) NOT NULL ,
    `date_envoi` TIMESTAMP NOT NULL ,
    `subject` VARCHAR(500) NOT NULL,
    CONSTRAINT FOREIGN KEY (`id_from`) REFERENCES `web_user`(`id`),
    CONSTRAINT FOREIGN KEY (`id_to`) REFERENCES `web_user`(`id`))
    ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci;
