<?php
	require_once("CommonAction.php");

	class UserAction extends CommonAction {
		public function __construct() {
				parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {

			if (isset($_GET["user_id"])) {
				$user_id = $_GET["user_id"];
			}
			else {
				$user_id = $_SESSION["user_id"];
			}


			// variables to be used in user page
			$this->user_id = $user_id;
			$this->username =  CommunityDAO::getUsernameFromId($user_id);
			$this->user_img = UserDAO::getUserImg($user_id);
			$this->user_karma = UserDAO::getUserKarma($user_id); 
			$this->communities = CommunityDAO::getUserCommunityList($user_id);
			$this->postInfo = PostDAO::getUserMessages($user_id);
			$this->usernameByid[] =NULL; 
			$this->communityName[] = NULL;

			for ($i=0; $i < sizeof($this->postInfo); $i++) {
				$this->usernameByid[$i] = CommunityDAO::getUsernameFromId($this->postInfo[$i]["user_id"]);
				$this->communityName[$i] = CommunityDAO::getCommuNameFromId($this->postInfo[$i]["community_id"]);
			}

			// Handling file upload for profile image
			//  Taken on http://www.w3schools.com/php/php_file_upload.asp
			if (isset($_FILES["image"])) {
				// $target_dir = "images/";
				$target_dir = realpath(dirname(getcwd()));
				$target_dir = $target_dir . "/images/";
				$fileBaseName = $_SESSION["user_id"] . uniqid($_SESSION["user_id"]) . substr(basename($_FILES["image"]["name"]), strpos(basename($_FILES["image"]["name"]), "."));    
				// $target_file = $target_dir . basename($_FILES["image"]["name"]);
				$target_file = $target_dir . $fileBaseName;
				$uploadOk = 1;
				$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
				// Check if image file is a actual image or fake image
				if(isset($_POST["submit"])) {
				    $check = getimagesize($_FILES["image"]["tmp_name"]);
				    if($check !== false) {
				        // echo "File is an image - " . $check["mime"] . ".";
				        $uploadOk = 1;
				    } else {
				        // echo "File is not an image.";
				        $uploadOk = 0;
				    }
				}
				// Check if file already exists
				if (file_exists($target_file)) {
				    // echo "Sorry, file already exists.";
				    $uploadOk = 0;
				}
				// Check file size
				if ($_FILES["image"]["size"] > 500000) {
				    // echo "Sorry, your file is too large.";
				    $uploadOk = 0;
				}
				// Allow certain file formats
				if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
				&& $imageFileType != "gif" && $imageFileType != "GIF" && $imageFileType != "JPEG"
				&& $imageFileType != "PNG" && $imageFileType != "JPG") {
				    // echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
				    $uploadOk = 0;
				}
				// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
				// if everything is ok, try to upload file
				} else {
				    if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
				        // echo "The file ". basename( $_FILES["image"]["name"]). " has been uploaded.";
				        // $pathOfIMG = "/read-it/images/" . basename($_FILES["image"]["name"]);
				        $pathOfIMG = "/read-it/images/" . $fileBaseName;
				        UserDAO::updateImg($pathOfIMG, $_SESSION["user_id"]);
				    } else {
				        // echo "Sorry, there was an error uploading your file.";
				    }
				}
				// echo ("<img src=" . "/read-it/images/" . basename($_FILES["image"]["name"]) . "></img>");
			}
		}
	}

