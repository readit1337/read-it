<?php
	require_once("CommonAction.php");

	class CommentsAction extends CommonAction {
		public function __construct() {
				parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {

			if(isset($_GET["post_id"])){
				$this->postInfo = CommentDAO::getPostInfo($_GET["post_id"]);
				$this->commentList = CommentDAO::getPostComments($_GET["post_id"]);

				$this->isUpvotedList = CommentDAO::getIfCommentupVotedOrDownvoted($this->commentList, $_SESSION["user_id"]);
				for($i = 1; $i <= count($this->commentList); $i++){
					$this->commentList[$i]["vote"] = 0;
					for($j = 0; $j < count($this->isUpvotedList); $j++){
						if($this->commentList[$i]["id"] == $this->isUpvotedList[$j]["comment_id"]){
							$this->commentList[$i]["vote"] = $this->isUpvotedList[$j]["vote"];
							break;
						}

					}
				}

			}
			else{
				 header('Location: ../../404.php');  
			}
			if(isset($_GET["community_id"])){
				$this->community = communityDAO::getCommunityInfo($_GET["community_id"]);
			}
			else{
				 header('Location: ../../404.php');  
			}

				if (isset($_POST["editor1"])) {
					$postInfo = CommentDAO::getPostInfo($_POST["hidden"]);
					$postInfo["commentaire"] = $_POST['editor1'];
					CommentDAO::postComment($postInfo["id"], $postInfo);
					header("Location: /read-it/r/post/comments.php?post_id=" . $_GET["post_id"] . "&community_id=" . $_GET["community_id"]);
				}
				if($_SESSION["visibility"]==2){
					if(isSet($_POST["comment_id_to_delete"])){
						commentDAO::deleteComment($_POST["comment_id_to_delete"]);
					}
				}




			}
		}
