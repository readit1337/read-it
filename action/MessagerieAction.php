<?php
	require_once("CommonAction.php");

	class MessagerieAction extends CommonAction {
		public function __construct() {
				parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		protected function executeAction() {
			if (!$_SESSION["logged"]) {
				header("Location:/read-it/index.php");
			}
			else{
				$this->listeUsers = UserDAO::getAllUsers();
				$this->messages = MessagerieDAO::getMessages($_SESSION["user_id"]);
				$this->sentMessages = MessagerieDAO::getOutMessages($_SESSION["user_id"]);
			}

			if (isset($_POST["user-select"]) && !is_null($_POST["user-select"]) && isset($_POST["subject"]) && !is_null($_POST["subject"]) && isset($_POST["message-body"]) && !is_null($_POST["message-body"])) {
				$data = [];
				$data["from_id"] = $_SESSION["user_id"];
				$data["to_id"] = $_POST["user-select"];
				$data["subject"] = $_POST["subject"];
				$data["message-body"] = $_POST["message-body"];

				$email= UserDAO::getEmailByUserId($data["to_id"]);

			

				$mail = new PHPMailer;
				$mail->isSMTP();
				$mail->Host = 'smtp.gmail.com';
				$mail->Port = 587;
				$mail->SMTPSecure = 'tls';
				$mail->SMTPAuth = true;
				$mail->Username = mailAccount;
				$mail->Password = mailPassword;
				$mail->addAddress($email);
				$mail->Subject = $_POST["subject"];
				$mail->msgHTML("Vous avez un nouveau message dans la messagierie interne de Read-it !");
				$mail->send();


				MessagerieDAO::newMessage($data);
			}
	


		}
	}
