<?php

	class CommentDao {
		public static function getPostInfo($postID) {
			$connection = Connection::getConnection();

			$statement = $connection-> prepare("select web_post.id,
			web_post.titre,
			web_post.inside_message,
			DATE_FORMAT(web_post.date_creation, '%D %M %Y %H:%m') AS date_creation,
			web_post.outside_link,
			web_post.score,
			web_post.user_id,
			web_post.community_id,
			web_community.name ,
			web_user.username
				FROM web_post
				JOIN web_user
				ON web_post.USER_ID = web_user.ID
        		JOIN web_community
       			ON web_post.community_id = web_community.ID
				WHERE web_post.ID = ?
				");

			/*AND web_post.COMMUNITY_ID = web_community.ID
			, web_community.NAME
			*/
			$statement->bindParam(1, $postID);

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$row = $statement->fetch();
			$unPost["id"] = $row["id"];
			$unPost["titre"] =$row["titre"];
			$unPost["inside_message"] =$row["inside_message"];
			$unPost["dateCreation"] =$row["date_creation"];
			$unPost["outsideLink"] =$row["outside_link"];
			$unPost["score"] =$row["score"];
			$unPost["userId"] =$row["user_id"];
			$unPost["communityID"] = $row["community_id"];
			$unPost["username"] =$row["username"];
			$unPost["communityName"] = $row["name"];
			return $unPost;
		}

		public static function getPostComments($postID) {
			$connection = Connection::getConnection();

			$statement = $connection-> prepare("SELECT web_comments.id,
			web_comments.community_id,
			web_comments.user_id,
			web_comments.post_id,
			DATE_FORMAT(web_post.date_creation, '%D %M %Y %H:%m') AS date_creation,
			web_comments.comment_text,
			web_comments.score,
			web_user.username
				FROM web_comments
				JOIN web_user
				ON web_comments.user_id = web_user.id
				WHERE post_id = ?
				");

			$statement->bindParam(1, $postID);

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$commentList = [];
			$i = 1;

			$rows = $statement->fetchAll();

			foreach($rows as $row) {
				$unComment["id"] =$row["id"];
				$unComment["community_id"] =$row["community_id"];
				$unComment["user_id"] =$row["user_id"];
				$unComment["post_id"] =$row["post_id"];
				$unComment["date_creation"] =$row["date_creation"];
				$unComment["comment_text"] =$row["comment_text"];
				$unComment["score"] =$row["score"];
				$unComment["username"] =$row["username"];
				$commentList[$i] = $unComment;
				$i++;
			}

			return $commentList;
		}

		public static function postComment($postID, $dataMsg) {
			$dataMsg["score"] = 0;
			$user_id = $_SESSION["user_id"];
			$connection = Connection::getConnection();
			$statement = $connection-> prepare("INSERT INTO WEB_COMMENTS(USER_ID, COMMUNITY_ID, POST_ID, COMMENT_TEXT, SCORE) VALUES (?, ?, ?, ?, ?)");

			$statement->bindParam(1, $user_id);
			$statement->bindParam(2, $dataMsg["communityID"]);
			$statement->bindParam(3, $postID);
			$statement->bindParam(4, $dataMsg["commentaire"]);
			$statement->bindParam(5, $dataMsg["score"]);

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
		}


		public static function upvotePost($user_id, $id, $idToVote) {
			$connection = Connection::getConnection();

			$statement = $connection-> prepare("select * from WEB_USER_POST where POST_ID = ? AND USER_ID = ?");
			$statement->bindParam(1, $id);
			$statement->bindParam(2, $user_id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			$count = $statement->fetchAll();


			if (count($count) == 0) {
				$statement = $connection-> prepare("INSERT INTO WEB_USER_POST(POST_ID, USER_ID, VOTE) VALUES (?, ?, ?)");

				$vote = "1";

				$statement->bindParam(1, $id);
				$statement->bindParam(2, $user_id);
				$statement->bindParam(3, $vote);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();

				$statement = $connection-> prepare("UPDATE WEB_POST SET SCORE=SCORE+1 WHERE ID=?");

				$statement->bindParam(1, $id);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				UserDAO::upKarma($idToVote);
			}
		}

		public static function downvotePost($user_id, $id, $idToVote) {
			$connection = Connection::getConnection();

			$statement = $connection-> prepare("select * from WEB_USER_POST where POST_ID = ? AND USER_ID = ?");
			$statement->bindParam(1, $id);
			$statement->bindParam(2, $user_id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			$count = $statement->fetchAll();

			if (count($count) == 0) {
				$statement = $connection-> prepare("INSERT INTO WEB_USER_POST(POST_ID, USER_ID, VOTE) VALUES (?, ?, ?)");

				$vote = "-1";

				$statement->bindParam(1, $id);
				$statement->bindParam(2, $user_id);
				$statement->bindParam(3, $vote);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();


				$statement = $connection-> prepare("UPDATE WEB_POST SET SCORE=SCORE-1 WHERE ID=?");

				$statement->bindParam(1, $id);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				UserDAO::downKarma($idToVote);
			}
		}


		public static function upvoteComment($user_id, $id, $idToVote) {
			$connection = Connection::getConnection();

			$statement = $connection-> prepare("SELECT * from WEB_USER_COMMENTS where COMMENT_ID = ? AND USER_ID = ?");
			$statement->bindParam(1, $id);
			$statement->bindParam(2, $user_id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			$count = $statement->fetchAll();

			if (count($count) == 0) {
				$statement = $connection-> prepare("INSERT INTO WEB_USER_COMMENTS(COMMENT_ID, USER_ID, VOTE) VALUES (?, ?, ?)");

				$vote = "1";

				$statement->bindParam(1, $id);
				$statement->bindParam(2, $user_id);
				$statement->bindParam(3, $vote);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();


				$statement = $connection-> prepare("UPDATE WEB_COMMENTS SET SCORE=SCORE+1 WHERE ID=?");

				$statement->bindParam(1, $id);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				UserDAO::upKarma($idToVote);
			}
		}

		public static function downvoteComment($user_id, $id, $idToVote) {
			$connection = Connection::getConnection();

			$statement = $connection-> prepare("SELECT * from WEB_USER_COMMENTS where COMMENT_ID = ? AND USER_ID = ?");
			$statement->bindParam(1, $id);
			$statement->bindParam(2, $user_id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			$count = $statement->fetchAll();

			if (count($count) == 0) {
				$statement = $connection-> prepare("INSERT INTO WEB_USER_COMMENTS(COMMENT_ID, USER_ID, VOTE) VALUES (?, ?, ?)");

				$vote = "-1";

				$statement->bindParam(1, $id);
				$statement->bindParam(2, $user_id);
				$statement->bindParam(3, $vote);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();

				$statement = $connection-> prepare("UPDATE WEB_COMMENTS SET SCORE=SCORE-1 WHERE ID=?");

				$statement->bindParam(1, $id);
				$statement->setFetchMode(PDO::FETCH_ASSOC);
				$statement->execute();
				UserDAO::downKarma($idToVote);
			}

		}
		public static function deleteComment($id) {
			$connection = Connection::getConnection();
			$statement = $connection-> prepare("DELETE FROM web_comments WHERE id=? ");
			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
		}


		public static function getIfCommentupVotedOrDownvoted($commentList, $userId){
			$commentVote = [];
			try{

			$connection = Connection::getConnection();
			$whichPost="( ";
			for($i=1; $i <= count($commentList); $i++){
				$whichPost = $whichPost .  $commentList[$i]["id"];
				if($i< count($commentList)){
					$whichPost = $whichPost . ", ";
				}else{
					$whichPost = $whichPost . " )";
				}

			}
			/*Obligatoire car le nombre de post est variable */
			$statement = $connection-> prepare( "SELECT vote,comment_id, user_id FROM web_user_comments WHERE comment_id IN " . $whichPost . "AND user_id = ". $userId );
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			$i = 0;
			while($row = $statement->fetch()) {
				$unPost["user_id"] =$row["user_id"];
				$unPost["comment_id"] = $row["comment_id"];
				$unPost["vote"] = $row["vote"];
				$commentVote[$i] = $unPost;
				$i++;
			}
			}
			catch(Exception $e){

			}
			return $commentVote;

		}

	}
