<?php

	class CommunityDAO {


		public static function getMessage($pageNumber, $community_id) {
			$connection = Connection::getConnection();

			$statement = $connection-> prepare("select * from web_post
			where community_id = ?
			ORDER BY date_creation
			LIMIT 25 OFFSET ?");
			//OFFSET ? ROWS FETCH NEXT 25 rows ONLY ");

			$statement->bindParam(1, $community_id);
			$statement->bindParam(2, $pageNumber*25);

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$postList = [];
			$i = 0;

			while($row = $statement->fetch()) {
				$unPost["likeMessage"] =$row["link_message"];
				$unPost["insideMessage"] =$row["link_message"];
				$unPost["dateCreation"] =$row["date_creation"];
				$unPost["outsideLink"] =$row["outside_link"];
				$unPost["score"] =$row["score "];
				$unPost["userId"] =$row["user_id"];
				$postList[$i] = $unPost;
				$i++;
			}
			return $postList;
		}



		public static function getCommunityList() {
			$connection = Connection::getConnection();

			$statement = $connection-> prepare("select * from web_community
			LIMIT 25 OFFSET 0");
			// OFFSET 0 ROWS FETCH NEXT 25 rows ONLY");

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$communityList = [];
			$i = 0;
			while($row = $statement->fetch()) {
				$uneCommunity["user_id"] =$row["user_id"];
				$uneCommunity["name"] = $row["name"];
				$uneCommunity["side_text"] =$row["side_text"];
				$uneCommunity["header_text"] =$row["header_text"];
				$uneCommunity["url"] = "/read-it/r/modeleCommunity.php?community_name=".$row["name"]."&community_id=".$row["id"];
				$communityList[$i] = $uneCommunity;
				$i++;
			}
			return $communityList;
		}

		public static function getUserCommunityList($user_id) {
			$connection = Connection::getConnection();
			$statement = $connection-> prepare("select web_user.id, user_id, name,  username from web_community, WEB_USER WHERE web_community.USER_ID=? AND web_community.user_id = web_user.id
			LIMIT 25 OFFSET 0");
			// OFFSET 0 ROWS FETCH NEXT 25 rows ONLY");

			$statement->bindParam(1, $user_id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);

			$statement->execute();

			$communityList = [];
			$i = 0;
			while($row = $statement->fetch()) {
				$uneCommunity["user_id"] =$row["user_id"];
				$uneCommunity["name"] = $row["name"];
				$uneCommunity["url"] = "/read-it/r/modeleCommunity.php?community_name=".$row["name"]."&community_id=".$row["id"];
				$uneCommunity["username"] = $row["username"];
				$communityList[$i] = $uneCommunity;
				$i++;
			}
			return $communityList;
		}

		public static function getCommunityInfo($id) {
			$connection = Connection::getConnection();

			$statement = $connection-> prepare("SELECT * from web_community WHERE ID = ?");
			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();


			$row = $statement->fetch();

			$community["name"] = $row["name"];
			$community["id"] = $row["id"];
			$community["user_id"] = $row["user_id"];
			$community["side_text"] = $row["side_text"];
			$community["header_text"] = $row["header_text"];

			return $community;
		}

		public static function getCommunityPostCount($id) {
			$connection = Connection::getConnection();

			$statement = $connection-> prepare("SELECT COUNT(*) as TOTAL from web_post WHERE community_id = ?");
			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();


			$postCount = $statement->fetch();

			return $postCount["TOTAL"];
		}

		public static function getAllCommunityPostCount() {
			$connection = Connection::getConnection();

			$statement = $connection-> prepare("SELECT COUNT(*) as TOTAL from web_post");
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();


			$postCount = $statement->fetch();

			return $postCount["TOTAL"];
		}

		public static function getPostsFromCommunity($community_id, $start, $count) {
			$connection = Connection::getConnection();
			$count++;
			$statement = $connection-> prepare("SELECT web_post.id,
				web_post.community_id,
				web_post.user_id,
				web_post.titre,
				web_post.inside_message,
				-- TO_CHAR(web_post.date_creation, 'DD-MON-YYYY HH24:MI') as date_creation,
				DATE_FORMAT(web_post.date_creation, '%D %M %Y %H:%m') AS date_creation,
				web_post.outside_link,
				web_post.score,
				web_user.username,
				web_community.name
				FROM web_post, web_user, web_community
				WHERE community_id = ? AND web_post.user_id = web_user.id AND web_post.community_id = web_community.id
				ORDER BY date_creation DESC
				LIMIT ? OFFSET ?");
				//OFFSET ? ROWS FETCH NEXT ? rows ONLY");



			$statement->bindParam(1, $community_id);
			$statement->bindParam(2, $count);
			$statement->bindParam(3, $start);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$postList = [];
			$i = 1;

			$rows = $statement->fetchAll();

			foreach($rows as $row) {
				$infoPost["id"] =$row["id"];
				$infoPost["community_id"] =$row["community_id"];
				$infoPost["user_id"] =$row["user_id"];
				$infoPost["titre"] = $row["titre"];
				$infoPost["inside_message"] =$row["inside_message"];
				$infoPost["date_creation"] =$row["date_creation"];
				$infoPost["outside_link"] =$row["outside_link"];
				$infoPost["score"] =$row["score"];
				$infoPost["username"] =$row["username"];
				$infoPost["nomCommunity"] =$row["name"];
				$postList[$i] = $infoPost;
				$i++;
			}

			return $postList;

		}

		public static function getAllPosts($start, $count) {
			$connection = Connection::getConnection();
			$count++;
			$statement = $connection-> prepare("SELECT web_post.id,
				web_post.community_id,
				web_post.user_id,
				web_post.titre,
				web_post.inside_message,
				-- DATE_FORMAT(web_post.date_creation, 'DD-MON-YYYY HH24:MI') as date_creation,
  			DATE_FORMAT(web_post.date_creation, '%D %M %Y %H:%m') AS date_creation,
				web_post.outside_link,
				web_post.score,
				web_user.username,
				web_community.name
				FROM web_post, web_user, web_community
				WHERE web_post.user_id = web_user.id AND web_post.community_id = web_community.id
				ORDER BY web_post.DATE_CREATION DESC
				LIMIT ? OFFSET ?");
				// OFFSET ? ROWS FETCH NEXT ? rows ONLY"
				// );


			// $statement->bindParam(1, $start);
			// $statement->bindParam(2, $count);

			$statement->bindParam(1, $count);
			$statement->bindParam(2, $start);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$postList = [];
			$i = 1;
			while($row = $statement->fetch()) {
				$infoPost["id"] =$row["id"];
				$infoPost["community_id"] =$row["community_id"];
				$infoPost["user_id"] =$row["user_id"];
				$infoPost["titre"] = $row["titre"];
				$infoPost["inside_message"] =$row["inside_message"];
				$infoPost["date_creation"] =$row["date_creation"];
				$infoPost["outside_link"] =$row["outside_link"];
				$infoPost["score"] =$row["score"];
				$infoPost["username"] =$row["username"];
				$infoPost["nomCommunity"] =$row["name"];
				$postList[$i] = $infoPost;
				$i++;
			}
			return $postList;

		}


		public static function getUsernameFromId($id) {
			$connection = Connection::getConnection();
			$statement = $connection -> prepare("SELECT web_user.username, web_user.id FROM web_user WHERE web_user.id = ?");

			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			$row = $statement->fetch();

			return $row["username"];
		}

		public static function getCommuNameFromId($id) {
			$connection = Connection::getConnection();
			$statement = $connection -> prepare("SELECT web_community.name, web_community.id FROM web_community WHERE web_community.id = ?");

			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			$row = $statement->fetch();

			return $row["name"];
		}



	}
