<?php

	class Connection {
		private static $connection;


		public static function getConnection() {
			if (Connection::$connection == null) {

        			//  COnnection A LECOLE
				//Connection::$connection = new PDO("oci:dbname=" . DB_NAME_ORACLE, DB_USER_ORACLE, DB_PASS_ORACLE);
				//Connection::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				//Connection::$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

				Connection::$connection = new PDO("mysql:host=localhost;dbname=" . DB_NAME_MARIA, DB_USER_MARIA, DB_PASS_MARIA);
				Connection::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				Connection::$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			}

			return Connection::$connection;
		}


		public static function closeConnection() {
			Connection::$connection = null;
		}

	}
