<?php

	class MessagerieDAO {
		public static function getMessages($id){
			$connection = Connection::getConnection();
			$statement = $connection-> prepare("SELECT * from WEB_MESSAGERIE where ID_TO = ? ORDER BY DATE_ENVOI DESC");
			$statement->bindParam(1, $id);

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$row = $statement->fetchAll();

			$messages = [];

			$i = 0;
			foreach ($row as $message) {
				$messages[$i]["id_to"] = $message["id_to"];
				$messages[$i]["id_from"] = $message["id_from"];
				$messages[$i]["date_envoi"] = $message["date_envoi"];
				$messages[$i]["message"] = $message["message"];
				$messages[$i]["subject"] = $message["subject"];
				$i++;
			}

			return $messages;
		}

		public static function getOutMessages($id){
			$connection = Connection::getConnection();
			$statement = $connection-> prepare("SELECT * from WEB_MESSAGERIE where ID_FROM = ? ORDER BY DATE_ENVOI DESC");
			$statement->bindParam(1, $id);

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$row = $statement->fetchAll();

			$messages = [];

			$i = 0;
			foreach ($row as $message) {
				$messages[$i]["id_to"] = $message["id_to"];
				$messages[$i]["id_from"] = $message["id_from"];
				$messages[$i]["date_envoi"] = $message["date_envoi"];
				$messages[$i]["message"] = $message["message"];
				$messages[$i]["subject"] = $message["subject"];
				$i++;
			}

			return $messages;
		}

		public static function newMessage($dataMsg){
			$connection = Connection::getConnection();
			$statement = $connection-> prepare("INSERT INTO WEB_MESSAGERIE(ID_FROM, ID_TO, MESSAGE, SUBJECT) VALUES (?, ?, ?, ?)");

			$statement->bindParam(1, $dataMsg["from_id"]);
			$statement->bindParam(2, $dataMsg["to_id"]);
			$statement->bindParam(3, $dataMsg["message-body"]);
			$statement->bindParam(4, $dataMsg["subject"]);

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
		}
	}
