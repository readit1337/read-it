<?php

	class UserDAO {

		public static function login($username, $password) {
			$connection = Connection::getConnection();

			$statement = $connection-> prepare("SELECT * FROM web_user WHERE username = ?");
			$statement->bindParam(1, $username);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$user = null;

			if ($row = $statement->fetch()) {
				if (password_verify($password, $row["password"])) {
					$user = $row;
					$_SESSION["id"] = $row["id"];
					$_SESSION["user_id"] = $row["id"];
					$_SESSION["logged"] = true;
					$_SESSION["visibility"] = $row["visibility"];
				}
			}

			return $user;
		}

		public function updateProfile($userId, $user) {
			$connection = Connection::getConnection();


		}

		public static function register($data) {
			$connection = Connection::getConnection();

			$email = $data["email"];
			$username = $data["username"];
			$password = $data["password"];
			$visibility = 1;
			$karma = 0;

			// Oracle PART
			$statement = $connection->prepare("INSERT INTO web_user(email, username, password, visibility, karma) VALUES (:email, :username, :password, :visibility, :karma)");
			$statement->bindParam(':email', $email);
			$statement->bindParam(':username', $username);
			$statement->bindParam(':password', $password);
			$statement->bindParam(':visibility', $visibility);
			$statement->bindParam(':karma', $karma);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();


			return "registration complete!";

		}

		public static function createCommunity($data) {
			$connection = Connection::getConnection();

			$titre = $data["titre"];
			$sideText = $data["sideText"];
			$description = $data["description"];
			$user_id = $_SESSION["id"];

			// Oracle PART
			$statement = $connection->prepare("INSERT INTO web_community(user_id, name, side_text, header_text) VALUES (:user_id, :name, :side_text, :header_text)");
			$statement->bindParam(':user_id', $user_id);
			$statement->bindParam(':name', $titre);
			$statement->bindParam(':side_text', $sideText);
			$statement->bindParam(':header_text', $description);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();


			return "registration complete!";

		}

		public static function getUserKarma($id) {
			$connection = Connection::getConnection();
			$statement = $connection -> prepare("SELECT karma, id FROM web_user WHERE id = ?");

			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			$row = $statement->fetch();

			return $row["KARMA"];
		}

		public static function getAllUsers() {
			$connection = Connection::getConnection();
			$statement = $connection -> prepare("SELECT username, id FROM web_user");
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			$row = $statement->fetchAll();

			$listeUsers = [];
			$i = 0;
			foreach ($row as $user) {
				$listeUsers[$i] = $user;
				$i++;
			}

			return $listeUsers;

		}

		public static function upKarma($id) {
			$connection = Connection::getConnection();
			$statement = $connection -> prepare("UPDATE web_user SET karma = karma+1 WHERE id = ?");
			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
		}

		public static function downKarma($id) {
			$connection = Connection::getConnection();
			$statement = $connection -> prepare("UPDATE web_user SET karma = karma-1 WHERE id = ?");
			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
		}


		public static function getEmailByUserId($user_id) {
			$connection = Connection::getConnection();
			$statement = $connection -> prepare("SELECT email FROM web_user WHERE id = ?");
			$statement->setFetchMode(PDO::FETCH_ASSOC);

			$statement->bindParam(1, $user_id);
			$statement->execute();
			$email = null;
			if ($row = $statement->fetch()) {
				$email =  $row["email"];

			}

			return $email;

		}


		public static function updateImg($path, $id) {
			$connection = Connection::getConnection();
			$statement = $connection -> prepare("UPDATE web_user SET IMG_PATH = ? WHERE id = ?");
			$statement->bindParam(1, $path);
			$statement->bindParam(2, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
		}

		public static function getImg($id) {
			$connection = Connection::getConnection();
			$statement = $connection -> prepare("SELECT IMG_PATH FROM web_user WHERE id = ?");
			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			$row = $statement->fetch();
			$_SESSION["img_path"] = $row["IMG_PATH"];
			if ($_SESSION["img_path"] != "") {
				return true;
			}
			else {
				return false;
			}
		}

		public static function getUserImg($id) {
			$connection = Connection::getConnection();
			$statement = $connection -> prepare("SELECT IMG_PATH FROM web_user WHERE id = ?");
			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			$row = $statement->fetch();
			$img = $row["IMG_PATH"];
			return $img;
		}
	}
