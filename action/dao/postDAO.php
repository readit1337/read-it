<?php

	class PostDAO {
		public static function getMessage($id_post){
			$connection = Connection::getConnection();
			$statement = $connection-> prepare("SELECT inside_message from web_post where id = ? ");
			$statement->bindParam(1, $id_post);

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			$lePost = null;
			if($row = $statement->fetch()) {
				$lePost["likeMessage"] =$row["inside_message"];
			}
			return $lePost;
		}

		public static function newMessage($dataMsg){
			$connection = Connection::getConnection();
			$statement = $connection-> prepare("INSERT INTO web_post(user_id, community_id,
				titre,
				inside_message,
				outside_link,
				score) VALUES (?, ?, ?, ?, ?, ?)");

			$statement->bindParam(1, $dataMsg["user_id"]);
			$statement->bindParam(2, $dataMsg["community_id"]);
			$statement->bindParam(3, $dataMsg["title"]);
			$statement->bindParam(4, $dataMsg["inside_message"]);
			$statement->bindParam(5, $dataMsg["outside_link"]);
			$statement->bindParam(6, $dataMsg["score"]);

			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();

			header('Location: /read-it/r/modeleCommunity.php?community_id='.$dataMsg["community_id"]);

		}

		public static function deletePost($id){
			$connection = Connection::getConnection();
			$statement = $connection-> prepare("DELETE FROM web_post WHERE id=? ");
			$statement->bindParam(1, $id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
		}

		public static function getUserMessages($user_id) {
			$connection = Connection::getConnection();
			$statement = $connection-> prepare("select web_post.id , inside_message, date_creation, user_id, titre, outside_link, score, community_id,  username from web_post, WEB_USER WHERE web_post.USER_ID=? AND web_post.user_id = web_user.id ORDER BY DATE_CREATION DESC
			LIMIT 25 OFFSET 0");
			//OFFSET 0 ROWS FETCH NEXT 25 rows ONLY");

			$statement->bindParam(1, $user_id);
			$statement->setFetchMode(PDO::FETCH_ASSOC);

			$statement->execute();

			$postList = [];
			$i = 0;
			while($row = $statement->fetch()) {
				$unPost["user_id"] =$row["user_id"];
				$unPost["titre"] = $row["titre"];
				$unPost["username"] = $row["username"];
				$unPost["outside_link"] = $row["outside_link"];
				$unPost["community_id"] = $row["community_id"];
				$unPost["inside_message"] = $row["inside_message"];
				$unPost["score"] = $row["score"];
				$unPost["dateCreation"] = $row["date_creation"];
				$unPost["id"] = $row["id"];
				$postList[$i] = $unPost;
				$i++;
			}
			return $postList;
		}

		public static function getIfPostupVotedOrDownvoted($postList, $userId){
			$postVote = [];
			try{

			$connection = Connection::getConnection();
			$whichPost="( ";
			for($i=1; $i <= count($postList); $i++){
				$whichPost = $whichPost .  $postList[$i]["id"];
				if($i< count($postList)){
					$whichPost = $whichPost . ", ";
				}else{
					$whichPost = $whichPost . " )";
				}

			}
			/*Obligatoire car le nombre de post est variable */
			$statement = $connection-> prepare( "SELECT vote,post_id, user_id FROM web_user_post WHERE post_id IN " . $whichPost . "AND user_id = ". $userId );
			$statement->setFetchMode(PDO::FETCH_ASSOC);
			$statement->execute();
			$i = 0;
			while($row = $statement->fetch()) {
				$unPost["user_id"] =$row["user_id"];
				$unPost["post_id"] = $row["post_id"];
				$unPost["vote"] = $row["vote"];
				$postVote[$i] = $unPost;
				$i++;
			}
			}
			catch(Exception $e){

			}
			return $postVote;

		}

	}
