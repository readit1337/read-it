<?php
	require_once("../action/CommonAction.php");

	class CommunityAction extends CommonAction {
		public function __construct() {
				parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		 protected function executeAction() {
		 	if(isset($_GET["community_id"])){
				$this->community = communityDAO::getCommunityInfo($_GET["community_id"]);
		 	}
		 	else{
		 		header('Location: ../404.php'); 
		 	}

		 	// Variables to be used in community page
		 	$this->createur = communityDAO::getUsernameFromId($this->community["user_id"]);
			$this->postString = "/read-it/r/post/nouveauMessage.php?community_id=" . $this->community["id"] . "&user_id=" . $this->community["user_id"];
			$this->communityPostCount = communityDAO::getCommunityPostCount($_GET["community_id"]);
			$this->postList = [];
			$this->page = 0;
			$this->start = 0;


			// Pages handling
			if (isset($_GET["pagenumber"])) {
				$this->start = ($_GET["pagenumber"]-1) * 25;
			}
			if ($this->communityPostCount>$this->start+25) {
				$this->hasNextPage = true;
			}
			else {
				$this->hasNextPage = false;
			}
			if ($this->hasNextPage) {
				$this->postList = communityDAO::getPostsFromCommunity($this->community["id"], $this->page*25 , $this->communityPostCount);
				if (isset($_SESSION["user_id"])) {
					$this->isUpvotedList = postDAO::getIfPostupVotedOrDownvoted($this->postList, $_SESSION["user_id"]);
				}
				for($i = 1; $i <= count($this->postList); $i++){
					$this->postList[$i]["vote"] = 0;
					if (isset($this->isUpvotedList)) {
						for($j = 0; $j < count($this->isUpvotedList); $j++){
							if($this->postList[$i]["id"] == $this->isUpvotedList[$j]["post_id"]){
								$this->postList[$i]["vote"] = $this->isUpvotedList[$j]["vote"];
								break;
							}
						}
					}
				}
			}else{
				$this->postList = communityDAO::getPostsFromCommunity($this->community["id"], $this->page*25 , $this->communityPostCount);
				if (isset($_SESSION["user_id"])) {
					$this->isUpvotedList = postDAO::getIfPostupVotedOrDownvoted($this->postList, $_SESSION["user_id"]);
				}
				for($i = 1; $i <= count($this->postList); $i++){
					$this->postList[$i]["vote"] = 0;
					if (isset($this->isUpvotedList)) {
						for($j = 0; $j < count($this->isUpvotedList); $j++){
							if($this->postList[$i]["id"] == $this->isUpvotedList[$j]["post_id"]){
								$this->postList[$i]["vote"] = $this->isUpvotedList[$j]["vote"];
								break;
							}
						}
					}
				}
			}

 	 	}
 	}
