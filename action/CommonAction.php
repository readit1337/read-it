<?php
session_start();
require_once("Constants.php");
require_once("dao/Connection.php");
require_once("dao/UserDAO.php");
require_once("dao/CommentDAO.php");
require_once("dao/communityDAO.php");
require_once("dao/postDAO.php");
require_once("dao/MessagerieDAO.php");
require_once("Translator.php");
require_once("PHPMailer-master/PHPMailerAutoload.php");

	abstract class CommonAction {
    public static $VISIBILITY_PUBLIC = 0;
    public static $VISIBILITY_MEMBER = 1;
    public static $VISIBILITY_MODERATOR = 2;
    public static $VISIBILITY_ADMINISTRATOR = 3;
    private $pageVisibility;

    public function __construct($pageVisibility) {
      $this->pageVisibility = $pageVisibility;
    }


    public final function execute() {
    	// TRANSLATOR
		if (empty($_SESSION["lang"])) {
			$_SESSION["lang"] = "fr";
		}

		if (!empty($_GET["lang"])) {
			$_SESSION["lang"] = $_GET["lang"];
		}
		if (!isset($_SESSION["logged"])) {
			$_SESSION["logged"] = false;
		}
		if (empty($_SESSION["visibility"])) {
			$_SESSION["visibility"] = CommonAction::$VISIBILITY_PUBLIC;
		}

		if ($_SESSION["visibility"] < $this->pageVisibility) {
			header("location: http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]?login-error=true");
			exit;
		}

		// REGEX pour verif fields valides
		$regexUsername = "/^.{6,}$/";
		$regexNom = "/^.{2,}$/";
		$regexPassword = "/^.{8,}$/";


		// HANDLE REGISTRATION HERE
		if (isset($_POST["hiddenRegistration"]) && $_POST["hiddenRegistration"] == "register") {
			$email = filter_var($_POST["email"], FILTER_VALIDATE_EMAIL);
			if(!$email === false &&
				preg_match($regexNom,$_POST["firstName"]) &&
				preg_match($regexNom,$_POST["lastName"]) &&
				preg_match($regexPassword,$_POST["password"]) &&
				preg_match($regexUsername,$_POST["reg-username"])
				){
					try {
						$data = array();
						$data["username"] = strtolower($_POST["reg-username"]);
						$data["password"] = password_hash($_POST["password"], PASSWORD_BCRYPT);
						$data["firstName"] = $_POST["firstName"];
						$data["lastName"] = $_POST["lastName"];
						$data["email"] = $_POST["email"];
						$reg = UserDAO::register($data);
					}
					catch (Exception $e) {

					}

					if ($reg == "registration complete!") {
						/************ENVOIE D'UN COURRIEL ***********/
						$mail = new PHPMailer;
						$mail->isSMTP();
						$mail->Host = 'smtp.gmail.com';
						$mail->Port = 587;
						$mail->SMTPSecure = 'tls';
						$mail->SMTPAuth = true;
						$mail->Username = mailAccount;
						$mail->Password = mailPassword;
						$mail->addAddress($_POST["email"]);
						$mail->Subject = mailSubject;
						$mail->msgHTML(mailMessage . $data["username"]);
						$mail->send();

						// redirect to home page with a registration success -- Strips unnecessary get param
						header('Location: http://' . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "?registration=success");
					}
					else {
						header('Location: http://' . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "?registration=failed");
					}
				}
			else {
				header('Location: http://' . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "?registration=champs-invalide");
			}
		}

		//HANDLE LOGIN HERE
		elseif (isset($_POST["hiddenLogin"]) && $_POST["hiddenLogin"] == "login") {

			$user = UserDAO::login(strtolower($_POST["username"]), $_POST["password"]);
			if (!$user) {
				$_SESSION["logged"] = false;
				if (empty($_GET)) {
					header('Location: http://' . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "?login=failed");
				}
				else {
					header('Location: http://' . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "&login=failed");
				}
			}
			else {
				$_SESSION["logged"] = true;
				if (empty($_GET)) {
					header('Location: http://' . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "?login=success");
				}
				else {
					header('Location: http://' . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "&login=success");
				}
			}
		}

		// HANDLE DECONNEXION HERE
		elseif (isset($_POST["hiddenLogout"]) && $_POST["hiddenLogout"] == "logout") {
			if ($_SESSION["logged"] == true) {
				session_unset();
				session_destroy();
				session_start();
				$_SESSION["visibility"] = CommonAction::$VISIBILITY_PUBLIC;
				$_SESSION["logged"] = false;
				$_SESSION["lang"] = "fr";
				$_SESSION["user_id"] = null;
				if (empty($_GET)) {
					header('Location: http://' . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "?logout=success");
				}
				else {
					header('Location: http://' . "$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "&logout=success");
				}
			}
		}


		$this->executeAction();
    }

		public function isLoggedIn() {
			return $_SESSION["visibility"] > CommonAction::$VISIBILITY_PUBLIC;
		}

		public function getUsername() {
			$username = "Invité";

			if ($this->isLoggedIn()) {
				$username = $_SESSION["username"];
			}

			return $username;
		}

    protected abstract function executeAction();


	}
