<?php 
require_once("Constants.php");
require_once("dao/Connection.php");
require_once("dao/UserDAO.php");
require_once("dao/CommentDAO.php");
require_once("dao/CommunityDAO.php");
require_once("dao/PostDAO.php");


$idToVote = $_POST["userToVote"];

// Handling upvote/ dvotes

if ($_POST["vote"] == "upvote" && $_POST["type"] == "post") {
	CommentDAO::upvotePost($_POST["user_id"], $_POST["id"], $idToVote);
	// UserDAO::upKarma($idToVote);
}
else if ($_POST["vote"] == "downvote" && $_POST["type"] == "post") {
	CommentDAO::downvotePost($_POST["user_id"], $_POST["id"], $idToVote);
	// UserDAO::downKarma($idToVote);
}
else if ($_POST["vote"] == "upvote" && $_POST["type"] == "comment") {
	CommentDAO::upvoteComment($_POST["user_id"], $_POST["id"], $idToVote);
	// UserDAO::upKarma($idToVote);
}
else if ($_POST["vote"] == "downvote" && $_POST["type"] == "comment") {
	CommentDAO::downvoteComment($_POST["user_id"], $_POST["id"], $idToVote);
	// UserDAO::downKarma($idToVote);
}
