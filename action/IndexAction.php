<?php
	require_once("action/CommonAction.php");

	class IndexAction extends CommonAction {

		public function __construct() {
				parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}

		
		protected function executeAction() {
			$this->communityPostCount = communityDAO::getAllCommunityPostCount();
			$this->start = 0;
			$this->hasNextPage = false;
			$this->postList = [];
			$this->isUpvotedList = [];

			// Handling many pages
			if (isset($_GET["pagenumber"])) {
				$this->start = ($_GET["pagenumber"]-1) * 25;
			}
			if ($this->communityPostCount>$this->start+25) {
				$this->hasNextPage = true;
			}
			if ($this->hasNextPage) {
				$this->postList = communityDAO::getAllPosts( $this->start, 24);
				if (isset($_SESSION["user_id"])) {
					$this->isUpvotedList = postDAO::getIfPostupVotedOrDownvoted($this->postList, $_SESSION["user_id"]);
				}
				// Ajout des status de vote si user connecte
				for($i = 1; $i <= count($this->postList); $i++){
					$this->postList[$i]["vote"] = 0;
					for($j = 0; $j < count($this->isUpvotedList); $j++){
						if($this->postList[$i]["id"] == $this->isUpvotedList[$j]["post_id"]){
							$this->postList[$i]["vote"] = $this->isUpvotedList[$j]["vote"];
							break;
						}

					}
				}
			}
			else {
				$this->postList = communityDAO::getAllPosts( $this->start, $this->communityPostCount);
				if (isset($_SESSION["user_id"])) {
					$this->isUpvotedList = postDAO::getIfPostupVotedOrDownvoted($this->postList, $_SESSION["user_id"]);
				}
				else {
					$this->isUpvotedList = [];
				}
				for($i = 1; $i <= count($this->postList); $i++){
					$this->postList[$i]["vote"] = 0;
					for($j = 0; $j < count($this->isUpvotedList); $j++){
						if($this->postList[$i]["id"] == $this->isUpvotedList[$j]["post_id"]){
							$this->postList[$i]["vote"] = $this->isUpvotedList[$j]["vote"];
							break;
						}

					}
				}
			}
			if($_SESSION["visibility"]==2){
				if(isSet($_POST["post_id_to_delete"])){
					postDAO::deletePost($_POST["post_id_to_delete"]);
				}
			}			
		}
	}