<?php
	require_once("action/CommonAction.php");

	class CreationCommunityAction extends CommonAction {
		public function __construct() {
				parent::__construct(CommonAction::$VISIBILITY_PUBLIC);
		}


		protected function executeAction() {
			if (isset($_POST["hiddenCreate"]) && $_POST["hiddenCreate"] == "create") {

				// Create a community
				$data = array();
				$data["titre"] = str_replace(" ", "_", $_POST["nom"]);
				$data["sideText"] = $_POST["sideText"];
				$data["description"] = $_POST["description"];
			
				$reg = UserDAO::createCommunity($data);

				if ($reg == "registration complete!") {
					header('Location: /read-it/index.php');
				}

			}

		}
	}
