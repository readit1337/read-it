<?php
require_once("action/CreationCommunityAction.php");
$action = new CreationCommunityAction();
$action->execute();
require_once("partial/header.php");
?>
	<main>
		<div>
			<div id="creationCommunity" class="creationCommunity">
				<form id = "newCommunityForm" class= "newCommunityForm" name="newCommunityForm" method="post">
          <input type="hidden" id="hiddenCreate" name="hiddenCreate" value="create">
          <legend>Nom de la community: </legend>
					<input type="text" name="nom">
          <legend>SideText de la community: </legend>
          <textarea form = "newCommunityForm" name="sideText" id="sideText"></textarea>
          <legend>Description de la community: </legend>
					<textarea form = "newCommunityForm" name="description" id="description"></textarea>
					<input type="submit">
				</form>
			</div>
		</div>
		 <script>
                CKEDITOR.replace( 'sideText' );
                CKEDITOR.replace( 'description' );
            </script>
	</main>

<?php
require_once("partial/footer.php");
