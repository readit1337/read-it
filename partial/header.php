<!DOCTYPE html>
<html>
	<head>
		<?php 	
		$trans = new Translator($_SESSION["lang"]);	
		?>
		<link rel="stylesheet" href="/read-it/css/style.css" media="screen" charset="utf-8">
		<script src="/read-it/js/modal.js" charset="utf-8"></script>
		<script src="/read-it/js/jquery-3.1.1.min.js" charset="utf-8"></script>
		<script src="/read-it/js/ajax.js" charset="utf-8"></script>
		<link rel="stylesheet" href="/read-it/jquery_ui/jquery-ui.min.css">
		<script src="/read-it/jquery_ui/jquery-ui.min.js"></script>
		<script src="/read-it/js/ckeditor/ckeditor.js"></script>
		<script src="/read-it/js/localStorage.js"></script>
		<meta charset="utf-8">
		<title>Index Read-It</title>
	</head>
	
	<body>
	<!-- Inspire de http://stackoverflow.com/questions/16941104/remove-a-parameter-to-the-url-with-javascript -->
	<script>
		function removeParam(parameter) {
		  var url=document.location.href;
		  var urlparts= url.split('?');

		 if (urlparts.length>=2) {
		   var urlBase=urlparts.shift(); 
		  var queryString=urlparts.join(); 

		  var prefix = encodeURIComponent(parameter)+'=';
		  var pars = queryString.split(/[&;]/g);
		  for (var i= pars.length; i-->0;)               
		      if (pars[i].lastIndexOf(prefix, 0)!==-1)   
		          pars.splice(i, 1);
		      if (pars.join() != "") {
		  		url = urlBase+'?'+pars.join('&');
		  	}
		  else {
		  	url = urlBase;
		  }
		   if (pars == null) {
		   	url = url.replace("?", "");
		   }
		  window.history.pushState('',document.title,url);
		}
		return url;
		}
	</script>
	<?php
if (isset($_GET["registration"])) {
	if ($_GET["registration"] == "success") {
		?>
		<script>window.onload = function () {$( "div.regsuccess" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );}
		removeParam("registration");
		</script>
		<?php
	}
	elseif ($_GET["registration"] == "failed") {
		?>
		<script>window.onload = function () {$( "div.regfailure" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );}
			removeParam("registration");
		</script>
		<?php
	}
	elseif ($_GET["registration"] == "champs-invalide") {
	?>
		<script>window.onload = function () {$( "div.champsfailed" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );}
			removeParam("registration");
		</script>
		<?php
	}		
}
if (isset($_GET["login"])) {
	if ($_GET["login"] == "success") {
		?>
		<script>window.onload = function () {$( "div.logsuccess" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );}
		removeParam("login");
		</script>
		<?php
	}
	elseif ($_GET["login"] == "failed") {
		?>
		<script>window.onload = function () {$( "div.logfailure" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );}
			removeParam("login");
		</script>
		<?php
	}
}

if (isset($_GET["logout"])) {
	if ($_GET["logout"] == "success") {
		?>
		<script>window.onload = function () {$( "div.logoutsuccess" ).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );}
		removeParam("logout");
		</script>
		<?php
	}
}



?>
<!-- Boxes for alerts if logged in or disconnected -->
<div class="alert-box regsuccess"><?= $trans->read("headerDuHtml", "regSuccessful") ?></div>
<div class="alert-box regfailure"><?= $trans->read("headerDuHtml", "regFailed") ?></div>
<div class="alert-box champsfailed"><?= $trans->read("headerDuHtml", "badFields") ?></div>
<div class="alert-box logsuccess"><?= $trans->read("headerDuHtml", "logSuccessful") ?></div>
<div class="alert-box logfailure"><?= $trans->read("headerDuHtml", "logFailed") ?></div>
<div class="alert-box logoutsuccess"><?= $trans->read("headerDuHtml", "logoutSuccessful") ?></div>
	<header>
	<nav class="navigation" id="navigation">
	<h3 id="menu">MENU</h3>
	<span><a href="/read-it/index.php"><img src="/read-it/images/read-it_logo.png" width="64px" height="64px"></a></span>
	<?php
	if ($_SESSION["logged"]) {
		if (UserDAO::getImg($_SESSION["user_id"]) == true) {
			?>
			<span><a href="/read-it/u/user.php"><img src=<?= $_SESSION["img_path"] ?> width="64px" height="64px"></a></span>
			<?php
		}
		else {
			?>
			<span><a href="/read-it/u/user.php"><img src="/read-it/images/account-logo.png" width="64px" height="64px"></a></span>
			<?php
		}
			?>
			<p><?= $trans->read("headerDuHtml", "changeImg") ?></p>
			<div id="changeImage" class="changeImage">
				<form action="/read-it/u/user.php" method="POST" enctype="multipart/form-data">
	         		<input type="file" name="image" />
	         		<input type="submit"/>
	      		</form>

			</div>
			<p><a href="/read-it/u/messagerie.php"><?= $trans->read("headerDuHtml", "messagerie") ?> </a></p>
		<?php
}
		if(!empty($_GET["lang"])){	
			if($_GET["lang"] === "fr"){
					$_SESSION["lang"] = "fr";
			}
			elseif($_GET["lang"] === "en"){
					$_SESSION["lang"] = "en";
			}
			else{
				$_SESSION["lang"] = "fr";
			}
		}
	 ?>
		<?php 
		if ( empty($_GET) ) {
				if($_SESSION["lang"] == "fr"){
						?>
						<div><a href= <?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "?lang=en" ?>>Anglais</a></div>
					<?php
				}elseif($_SESSION["lang"] == "en"){
					?>
					<div><a href=<?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "?lang=fr" ?>>Francais</a></div>
					<?php  		
				}
			}
			else {
				if (strpos("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", "?lang=en") || strpos("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]", "?lang=fr")) {
					if($_SESSION["lang"] == "en"){
						?>
						<div><a href= <?= str_ireplace("?lang=en" , "?lang=fr", "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") ?>>Francais</a></div>
					<?php
				}elseif($_SESSION["lang"] == "fr"){
					?>
					<div><a href=<?= str_ireplace("?lang=fr", "?lang=en", "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") ?>>Anglais</a></div>
					<?php  		
				}
				}
				else {
				if($_SESSION["lang"] == "en" && isset($_GET["lang"])){
						?>
						<div><a href= <?= str_ireplace("&lang=en" , "&lang=fr", "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") ?>>Francais</a></div>
					<?php

				}elseif($_SESSION["lang"] == "fr" && isset($_GET["lang"])){
					?>
					<div><a href=<?= str_ireplace("&lang=fr", "&lang=en", "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]") ?>>Anglais</a></div>
					<?php  		
				}
				if(!isset($_GET["lang"]) && $_SESSION["lang"] == "en"){
						?>
						<div><a href= <?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "&lang=fr" ?>>Francais</a></div>
					<?php

				}elseif(!isset($_GET["lang"]) && $_SESSION["lang"] == "fr"){
					?>
					<div><a href= <?= "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" . "&lang=en" ?>>Anglais</a></div>
					<?php  		
				}
			}
		}
		?>
		<h4 id="menuCommunity">COMMUNAUTÉ</h4>
		<div id="communautes" class="communities">
			<ul>
				<?php
					$communities = CommunityDAO::getCommunityList();
					for ($i=0; $i < sizeof($communities); $i++) {
						?>
						<li><a href=<?= $communities[$i]["url"] ?> target=""> <?= $communities[$i]["name"] ?> </a></li>
						<?php
					}
				?>
			</ul>
		</div>
		<?php
			if ($_SESSION["logged"] == false) {
		?>
		<button id="registerBtn" class="button" onclick="registerOpen()"><?= $trans->read("headerDuHtml", "btnRegister")?></button>
		<!-- The Registration Modal -->
		<div id="myRegisterModal" class="registerModal">
			<!-- Registration Modal content -->
			<div class="register-modal-content">
				<span class="registerClose">x</span>
				<form id='register' action='' method='post' accept-charset='UTF-8'>
					<fieldset>
						<legend><?= $trans->read("headerDuHtml", "legendRegis") ?></legend>
						<input type='hidden' name='hiddenRegistration' id='hiddenRegistration' value='register'/>
						<label for='firstName' ><?= $trans->read("headerDuHtml", "fName") ?></label>
						<input type='text' name='firstName' id='firstName' maxlength="50" />
						<label for='lastName' ><?= $trans->read("headerDuHtml", "lName") ?></label>
						<input type='text' name='lastName' id='lastName' maxlength="50" />
						<label for='email' >Email: </label>
						<input type='text' name='email' id='email' maxlength="160" />
						<label for='username' ><?= $trans->read("headerDuHtml", "uName")?></label>
						<input type='text' name='reg-username' id='reg-username' maxlength="50" />
						<label for='password' ><?= $trans->read("headerDuHtml", "password")?></label>
						<input type='password' name='password' id='password' maxlength="50" />
						<input type='submit' name='Submit' value='Register' />
					</fieldset>
				</form>
			</div>
		</div>
    <!-- Trigger/Open The Login Modal -->
		<button id="loginBtn" class="button" onclick="loginOpen();"><?= $trans->read("headerDuHtml", "btnLogin") ?></button>
		<!-- The Modal -->
		<div id="myLoginModal" class="loginModal">
			<!-- Login Modal content -->
			<div class="login-modal-content">
				<span class="loginClose">x</span>
				<form id='login' action='' method='post' accept-charset='UTF-8'>
					<fieldset >
						<legend>Login</legend>
						
						<input type='hidden' name='hiddenLogin' id='hiddenLogin' value='login'/>
						<label for='username' ><?= $trans->read("headerDuHtml", "uName") ?></label>
						<input type='text' name='username' id='username' maxlength="50" value=""/>
						<label for='password' ><?= $trans->read("headerDuHtml", "password") ?></label>
						<input type='password' name='password' id='password' maxlength="50" />
						<input type='submit' name='Submit' id="login-submit" value='Login' onclick="saveMsg(this.form.username.value)" />
					</fieldset>
				</form>
			</div>
		</div>
	<?php
	}
		if ($_SESSION["logged"] == true) {
	?>
	<a href="/read-it/u/user.php"><?= $trans->read("headerDuHtml", "account") ?> </a>
    <form class="logout" action="" method="post">
      	<input type='hidden' name='hiddenLogout' id='hiddenLogout' value='logout'/>
        <input class = "btnDeconnexion "type='submit' name='Submit' value=<?= $trans->read("headerDuHtml", "btnLogoff") ?> />
    </form>
 	<form class="create-community" action="/read-it/creationCommunity.php" method="post">
      	<input type='hidden' name='hiddenCreateCommunity' id='hiddenCreateCommunity' value='createCommunity'/>
        <input class = "button" type='submit' name='Submit' value=<?= $trans->read("headerDuHtml", "createCommunity") ?> />
    </form>
    <?php
	}
	 ?>
	</nav>		
	</header>
	