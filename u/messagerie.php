<?php
require_once("../action/MessagerieAction.php");
$action = new MessagerieAction();
$action->execute();
require_once("../partial/header.php");

$listeUsers = $action->listeUsers;
$messages = $action->messages;
$sentMessages = $action->sentMessages;
?>
<main>
<div id="new-message">
<h1><?= $trans->read("messagerie", "newMessage") ?></h1>
	<form name="send-message" action="messagerie.php" id="send-message" method="POST">
		<select name="user-select">
        	<?php

        		if (isset($_GET["user_id"])) {
        			$selectUser = $_GET["user_id"];
        		}
        		foreach ($listeUsers as $user) {
        			if (isset($selectUser)) {
        				if ($selectUser == $user["ID"]) {
						?>
        					<option value=<?= $user["ID"] ?> selected><?= $user["USERNAME"] ?></option>
        				<?php
        				}
        				else {
    					?>
        					<option value=<?= $user["ID"] ?>><?= $user["USERNAME"] ?></option>
        				<?php
        				}
        			}
        			else {
	        			?>
	        			<option value=<?= $user["ID"] ?>><?= $user["USERNAME"] ?></option>
	        			<?php
        			}
        		}

        	?>
    	</select>
		<input type="text" name="subject"></input>
		<div>
			
		<textarea form="send-message" name="message-body" cols="50" rows="6" id="messageArea" class="clearboth"></textarea>
		</div>
		<input type="submit"></input>
	</form>
</div>

<h3 class="titleMessenger"><?= $trans->read("messagerie", "inbox") ?></h3>
<div id="inbox">
<!-- Loop pour display les messages recus -->
	<?php
		for ($i=0; $i < count($messages); $i++) { 
				?>
				<div class="message">
					<p class="message-subject">
						<?= $trans->read("messagerie", "sujet") ?><?= $messages[$i]["subject"]?>
					</p>
					<p class="message-body">
						<?= $trans->read("messagerie", "message") ?><?= $messages[$i]["message"]?>
					</p>
					<p class="message-from">
						<?= $trans->read("messagerie", "from") ?><?= communityDAO::getUsernameFromId($messages[$i]["id_from"]) ?>
					</p>
				</div>

				<?php
		}	
	?>
</div>

<h3 class="titleMessenger"><?= $trans->read("messagerie", "outbox") ?></h3>
<div id="outbox">
<!-- Loop pour display les messages recus -->
	<?php
		for ($i=0; $i < count($sentMessages); $i++) { 
				?>
				<div class="message">
					<p class="message-subject">
						<?= $trans->read("messagerie", "sujet") ?><?= $sentMessages[$i]["subject"]?>
					</p>
					<p class="message-body">
						<?= $trans->read("messagerie", "message") ?><?= $sentMessages[$i]["message"]?>
					</p>
					<p class="message-to">
						<?= $trans->read("messagerie", "to") ?><?= communityDAO::getUsernameFromId($sentMessages[$i]["id_to"]) ?>
					</p>
				</div>
				<?php
		}	
	?>
</div>



</main>
<?php  
require_once("../partial/footer.php");
