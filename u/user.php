<?php
require_once("../action/UserAction.php");
$action = new UserAction();
$action->execute();
require_once("../partial/header.php");



$user_id = $action->user_id;
$username = $action->username;
$user_img = $action->user_img;
$user_karma = $action->user_karma;
$communities = $action->communities;
$postInfo = $action->postInfo;
$usernameByid = $action->usernameByid; 
$communityName = $action->communityName;

?>

<main>
	<div id ="subHeader"> 
		<h1 id="communityName"><?= $trans->read("user", "header") ?> <?= $username ?> </h1> 
		<?php
		if (isset($_SESSION["user_id"])) {
			if ($_SESSION["user_id"] > 0) {
				?>
				<p><a href=<?= "/read-it/u/messagerie.php?user_id=" . $user_id?>> <?= $trans->read("user", "envoyerMessage") ?></a></p>
				<?php
			}
		}
		?>
		<p id="headerText">
			<?= $trans->read("user", "accountMessage") ?>
		</p>
		<p>
			Karma :  <?= $user_karma ?>
		</p>

		<div class="user-img-wrapper">
			<?php
			if ($user_img != null) {
				$userURL = "/read-it/u/user.php?user_id=" . $user_id;
				?>
				<span><a href=<?= $userURL?>><img src=<?= $user_img?> width="128px" height="128px"></a></span>
				<?php
			}
			else {
				$user_img = "/read-it/images/account-logo.png";
				$userURL = "/read-it/u/user.php?user_id=" . $user_id;
				?>
				<span><a href=<?= $userURL?>><img src=<?= $user_img?> width="128px" height="128px"></a></span>
				<?php
			}

			?>
		</div>
	</div>

	<div id="communities-wrapper" class="communities-wrapper">
		<div id="communautes" class="communities">
			<h3><?= $trans->read("user", "accountCommunities") ?></h3>
			<ul>
				<?php
					
					for ($i=0; $i < sizeof($communities); $i++) {
						?>
						<li><a href=<?= $communities[$i]["url"] ?> target=""> <?= $communities[$i]["name"] ?> </a></li>
						<?php
					}
				?>
			</ul>
		</div>
	</div>


	<div id="posts-wrapper-userpage" class="posts-wrapper">
		<div id="posts" class="posts">
			<h3><?= $trans->read("user", "accountPosts") ?></h3>





		<?php
			
			for ($i=0; $i < sizeof($postInfo); $i++) {
				?>
		<div class="aPost-userpage"><!--injecter php -->
				<div class = "leftPostSection">
				<!--<div class="noPost"><?= $postList[$i]["inside_message"]?></div> -->

					
					<span class="rank"></span><span> <?= $postInfo[$i]["score"]?> </span>
				
				</div>
					
				<div class = "rightPostSection">
						<h3 id="titlePost">
							<a href=""><?= htmlentities($postInfo[$i]["titre"])?></a>
						</h3>
						<p class="tagline">soumis le <?= $postInfo[$i]["dateCreation"]?> par <?= $usernameByid[$i] ?> dans /r/<?=  $communityName[$i] ?></p>
						<div id="messageWrapper">
							<p id=messsage><?= $postInfo[$i]["inside_message"]?></p>
						</div>
						<a class = "comments" href="">COMMENTAIRE</a>
				</div>
		</div>
<?php 
} 
?>



		</div>
	</div>	



</main>
<?php  
require_once("../partial/footer.php");
