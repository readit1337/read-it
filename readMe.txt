Read-it, par �tienne Fullum et Thomas drolet. 
Pr�sent� � : Fr�d�ric Th�riault 

#Description g�n�ral
Nous avons con�u un site web semblable � reddit.com. Notre site web permet � un 
utilisateur de ce creer un compte, int�ragir avec des communaut�s modifier son profils, 
soumettre des soumissions vers un lien externe (par exemple un article de journal) ou 
vers un lien interne (son propre texte). L'utilisateur peut aussi voter les soumissions 
(similaire � un like/dislike) des autres utilisateurs. Les soumissions donc ont un score. Un 
utilisateur peut aussi commenter une soumission. Les commentaire aussi ont un score. Quand 
un utilisateur se fait voter sa soumission ou son commentaire, il gagne ou perd du "karma". 
Un utilisateur peuvent voir le karma d'un autre utilisateur via son profil. 

#�bauche des fonctionnalit�s : 
-Design du site web enti�rement faite � la main sans framework du genre bootstrap;
-Convivial et intuitif;
-inscription au site web (avec encryption du mot de passe et v�rification des champs via des REGEX);
-Ajout de soumission;
-retrait de soumission par un admin;
-ajout d'un commentaire;
-retrait d'un commentaire (par l'admin);
-Possibilit� de upvote/downvote une soumission;
-Possibilit� de upvote/downvote un commentaire;
-T�l�versement  d'une image comme photo de profils ;
-CkEditor pour les commentaire et la creation de communaut�;
-Soutenue par une base de donn�es ORACLE rapide et index� ;
-Site web internationalis�;
-Effet javascript : fen�tre modal et Ajax;
-Module d'envoie d'un courriel de confirmation lors de l'inscription (� l'aide de PHPMAIL);
-page 404 sur mesure;
-Syst�me de messagerie interne priv� 
-Appuiller sur un utilisateur pour voir une historique de ses activit�s ainsi que ses communaut�s cr��s
-redirection si mauvais param�tre GET � 404.PHP
-affichage relative au nombre de post du bouton suivant


#Information compl�mentaire 

#Le dossier _db est rest� dans le dossier racine pour la remise car nos chemins d'acc�s 
dans notre code PHP/JS deviennent probl�matique si l'on modifie le dossier racine. 
Nous avons donc mis le script de la base de donn�e dans un zip encrypt�. En production, le 
dossier contenant le scripte de la base de donn�es n'aurais jamais �t� dans le m�me dossier 
que le site web en soi. 

Le mot de passe est : 
AifiPie0caisahpij,u;ayeEzashgooxahgoosahchui0AisheI5oojee7wo6aIcoh^w4phai'r5shieneehiam6ateE4Je\Sod5shAe*foop1aIhah6mai6naett]oa=bo&i0ceEbae6sHi0pha5chaideid3ah7nah1eec2wah0roopie8leimAhthah3aojOhlo(A:hioegh9eich7siij.aeth8ievahjee8ief1oHzu5ixOox0hu0oogh7

###gmail pour l'envoie de confirmation lors de la cr�ation du compte: 
read.it@gmail.com /////// AAAaaa222
###Utilisateur qui peut supprimer des posts et commentaire (admin) : 
administrateur /////// AAAaaa222

