// Handling upvotes and downvotes Ajax. but reloading page after cause logic.
// Manque de temps pour aller get les changements et changer l'image de upvote/downvote par ajax

function upvotePost(e, id, user_id, userToVote) {
	console.log("upvote: " + id);
	e.preventDefault();
	$.ajax({
	    type: 'POST',
	    url: '/read-it/action/voting.php',
	    data: { 
	        'id': id,
	        'vote': "upvote",
	        'type': "post",
	        'user_id' : user_id,
	        'userToVote' : userToVote
	    },
	    success: function(){
	        location.reload();
	    }
	});

}

function downvotePost(e, id, user_id, userToVote) {
	console.log("downvote: " + id);
	e.preventDefault();
	$.ajax({
    type: 'POST',
    url: '/read-it/action/voting.php',
    data: { 
        'id': id,
        'vote': "downvote",
        'type': "post",
        'user_id' : user_id,
	    'userToVote' : userToVote

    },
    success: function(){
        location.reload();
    }
});


}

function upvoteComment(e, id, user_id, userToVote) {
	console.log("upvote: " + id);
	e.preventDefault();
	$.ajax({
    type: 'POST',
    url: '/read-it/action/voting.php',
    data: { 
        'id': id,
        'vote': "upvote",
        'type': "comment",
        'user_id' : user_id,
        'userToVote' : userToVote	
    },
    success: function(){
        location.reload();
    }
});
}

function downvoteComment(e, id, user_id, userToVote) {
	console.log("downvote: " + id);
	e.preventDefault();
	$.ajax({
    type: 'POST',
    url: '/read-it/action/voting.php',
    data: { 
        'id': id,
        'vote': "downvote",
        'type': "comment",
        'user_id' : user_id,
        'userToVote' : userToVote
    },
    success: function(){
        location.reload();
    }
});
}