
// Modal javascript pop-ups

function registerOpen() {
  var modal = document.getElementById('myRegisterModal');
  var btn = document.getElementById("myRegisterBtn");
  var span = document.getElementsByClassName("registerClose")[0];
  modal.style.display = "block";
  span.onclick = function() {
      modal.style.display = "none";
  }
  window.onclick = function(event) {
      if (event.target == modal) {
          modal.style.display = "none";
      }
  }
}

function loginOpen() {
  var modal = document.getElementById('myLoginModal');
  var btn = document.getElementById("myLoginBtn");
  var span = document.getElementsByClassName("loginClose")[0];
  modal.style.display = "block";
  // Call load to localstorage
  setUserName();
  span.onclick = function() {
      modal.style.display = "none";
  }
  window.onclick = function(event) {
      if (event.target == modal) {
          modal.style.display = "none";
      }

  }
}
